#!/usr/bin/env python

import json
import sys
import io
import re
import os

reload(sys)
sys.setdefaultencoding("latin-1")

from lib.sentence import Sentence
from lib.sentencepair import SentencePair
from lib.chunk import Chunk
from lib.annotation import Annotation
from lib.word2vec import initialize_vocabulary


def _open_file(filename):
    return io.open(filename, 'r', encoding='latin_1')


def _get_sentences(sentence_config):

    sentences = []

    plain = _open_file(sentence_config['plain'])
    chunked = _open_file(sentence_config['chunked'])
    if os.path.isfile(sentence_config['annotated']):
        annotated = _open_file(sentence_config['annotated'])
    else:
        annotated = ()

    for line in plain:
        sentences.append(Sentence(line.strip()))

    for i, line in enumerate(chunked):
        sentence = sentences[i]
        chunk_index = 0
        for chunk in re.split('(?: ?\[ ?)|(?: ?\] ?)', line):
            chunk = chunk.strip()
            if len(chunk) != 0:
                sentence.chunks.append(Chunk(chunk_index, chunk))
                chunk_index += len(chunk.split())

    for line in annotated:
        id_match = re.match('(\d+):', line)
        if not id_match:
            continue
        sentence = sentences[int(id_match.group(1))]
        annotation_set = []
        parts = re.findall('(?:\\[)[^\\[\\]]+(?:\\])', line)
        for part in parts:
            annotation_set.append(Annotation(part))
        if len(annotation_set) != 0:
            sentence.annotations.append(annotation_set)

    plain.close()
    chunked.close()
    if os.path.isfile(sentence_config['annotated']):
        annotated.close()

    return sentences


def _get_sentence_pairs(config):
    result = []
    a_sentences = _get_sentences(config['a'])
    b_sentences = _get_sentences(config['b'])
    i = 0
    for a_sentence, b_sentence in zip(a_sentences, b_sentences):
        i += 1
        pair = SentencePair(i, a_sentence, b_sentence)
        result.append(pair)
    return result


def _load_config(config_filename):
    with open(config_filename, 'r') as config_file:
        return json.load(config_file)


def main(config_filename):
    config = _load_config(config_filename)
    initialize_vocabulary(config['word2vec_bin'])
    pairs = _get_sentence_pairs(config)
    for pair in pairs:
        print pair
        print
        print

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'usage: python semeval.py path/to/run/configuration.json'
        exit(1)
    main(sys.argv[1])
