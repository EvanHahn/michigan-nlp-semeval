#!/usr/bin/env bash
# This runs an evaluation with a run configuration that you specify,
# or ALL run configurations if you pass no arguments.

set -e

evaluate() {

  local rn="$(date '+%F_%H-%M-%S')"
  local name="$rn,$1"
  local wa_file="tmp/$name.wa"
  local eval_file="tmp/$name,eval.txt"

  echo -e "evaluation for \e[00;36m$1\e[00m:"

  local start_time="$(date '+%s')"

  mkdir -p tmp
  ./semeval.py "run-configurations/$1.json" > "$wa_file"
  training-data/evalF1.pl training-data/STSint.gs.headlines.wa "$wa_file" > "$eval_file"

  cat "$eval_file"

  local end_time="$(date '+%s')"
  echo
  echo -n 'took '
  echo -n $(( $end_time - $start_time ))
  echo ' seconds'

}

source env/bin/activate
set -u

if [ $# -eq 0 ]; then
  evaluate test-ours-annotated-headlines
  evaluate test-ours-annotated-images
  evaluate test-ours-not-annotated-headlines
  evaluate test-ours-not-annotated-images
  evaluate test-theirs-annotated-headlines
  evaluate test-theirs-annotated-images
  evaluate test-theirs-not-annotated-headlines
  evaluate test-theirs-not-annotated-images
  evaluate train-ours-annotated-headlines
  evaluate train-ours-annotated-images
  evaluate train-ours-not-annotated-headlines
  evaluate train-ours-not-annotated-images
  evaluate train-theirs-annotated-headlines
  evaluate train-theirs-annotated-images
  evaluate train-theirs-not-annotated-headlines
  evaluate train-theirs-not-annotated-images
else
  evaluate "$1"
fi
