from alignment import Alignment

ALIC_THRESHOLD = 0.8


class SentencePair(object):

    def __init__(self, id, a_sentence, b_sentence):
        self.id = id
        self.a = a_sentence
        self.b = b_sentence

    def alignments(self):

        result = []

        a = self.a
        b = self.b
        a_chunks = self.a.chunks[:]
        b_chunks = self.b.chunks[:]

        while a_chunks and b_chunks:

            a_to_remove = b_to_remove = None
            best_alignment = Alignment(None, a, None, b)
            best_alignment.score = -1

            for a_index, a_chunk in enumerate(a_chunks):
                for b_index, b_chunk in enumerate(b_chunks):
                    alignment = Alignment(a_chunk, a, b_chunk, b)
                    if ((best_alignment.score < alignment.score) and
                            (alignment.type != 'NOALI')):
                        a_to_remove = a_index
                        b_to_remove = b_index
                        best_alignment = alignment

            if best_alignment.score == -1:
                break
            result.append(best_alignment)
            del a_chunks[a_to_remove]
            del b_chunks[b_to_remove]

        result += self.parse_remaining(a_chunks, True)
        result += self.parse_remaining(b_chunks, False)

        return result

    def parse_remaining(self, unaligned_chunks, is_a):
        result = []
        a = self.a
        b = self.b
        other_chunks = (b if is_a else a).chunks[:]
        for unaligned_chunk in unaligned_chunks:
            best_alignment = Alignment(None, a, None, b)
            for other_chunk in other_chunks:
                if is_a:
                    alignment = Alignment(unaligned_chunk, a, other_chunk, b)
                else:
                    alignment = Alignment(other_chunk, a, unaligned_chunk, b)
                if alignment.score > best_alignment.score:
                    best_alignment = alignment
            if best_alignment.score < ALIC_THRESHOLD:
                if is_a:
                    best_alignment = Alignment(unaligned_chunk, a, None, b)
                else:
                    best_alignment = Alignment(None, a, unaligned_chunk, b)
            else:
                best_alignment.type = 'ALIC'
                if is_a:
                    best_alignment.b_chunk = None
                else:
                    best_alignment.a_chunk = None
            result.append(best_alignment)
        return result

    def __str__(self):
        return u'\n'.join([
            '<sentence id="' + str(self.id) + '" status="">',
            '// ' + self.a.plain,
            '// ' + self.b.plain,
            '<source>',
            self.xml_chunk_output(self.a),
            '</source>',
            '<translation>',
            self.xml_chunk_output(self.b),
            '</translation>',
            '<alignment>',
            self.alignment_output(),
            '</alignment>',
            '</sentence>',
        ])

    def xml_chunk_output(self, sentence):
        result = []
        i = 0
        for chunk in sentence.chunks:
            for word in chunk.words.split():
                word = unicode(word)
                i += 1
                result.append(u' '.join((str(i), word, ': ')))
        return u'\n'.join(result)

    def alignment_output(self):
        alignments = self.alignments()
        result = []
        for alignment in alignments:
            result.append(unicode(alignment))
        return u'\n'.join(result)
