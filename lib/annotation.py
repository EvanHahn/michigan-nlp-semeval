class Annotation(object):

    def __init__(self, input):
        split = input[1:-1].strip().split()
        self.type = split[0]
        self.words = split[1:]
        self.string = ' '.join(self.words)

    def __str__(self):
        return '{' + self.type + ': ' + str(self.words) + '}'

    def __repr__(self):
        return str(self)
