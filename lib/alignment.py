from sets import Set
import compare_words

THRESHOLDS = {
    'EQUI': 0.9,
    'SIMI': 0.4,
    'NOALI': 0.2
}


class Alignment(object):

    def __init__(self, a_chunk, a, b_chunk, b):

        self.a_chunk = a_chunk
        self.b_chunk = b_chunk
        self.a = a
        self.b = b

        self.type = None

        if (a_chunk is None) or (b_chunk is None):
            self.type = 'NOALI'
            self.score = 0.0
        elif self.close_enough():
            self.type = 'EQUI'
            self.score = 1.0
        if self.type is not None:
            return

        a_words = a_chunk.words.split()
        b_words = b_chunk.words.split()

        confidence = 0.0
        max_confidence = len(a_words)

        while a_words and b_words:
            best_a_index = best_b_index = None
            best_score = -1
            for a_i, a_word in enumerate(a_words):
                for b_i, b_word in enumerate(b_words):
                    if (len(a_word) == 1) or (len(b_word) == 1):
                        max_distance = 0
                    else:
                        max_distance = 1
                    is_close = compare_words.close_enough(a_word, b_word,
                                                          max_distance)
                    if is_close or compare_words.has_synset_overlap(a_word,
                                                                    b_word):
                        best_a_index = a_i
                        best_b_index = b_i
                        best_score = 1.0
                    elif best_score != 1.0:
                        score = compare_words.score(a_word, b_word)
                        if score > best_score:
                            best_a_index = a_i
                            best_b_index = b_i
                            best_score = score

            if best_score <= 0:
                break
            del a_words[best_a_index]
            del b_words[best_b_index]
            confidence += best_score

        if max_confidence != 0:
            self.score = confidence / max_confidence
        else:
            self.score = 0.0

        if self.has_same_annotation_type():
            self.score += 0.5
        self.score = min(self.score, 1.0)

        if self.score < THRESHOLDS['NOALI']:
            self.type = 'NOALI'
        elif self.score > THRESHOLDS['EQUI']:
            self.type = 'EQUI'
        elif self.score > THRESHOLDS['SIMI']:
            self.type = 'SIMI'
        else:
            self.type = 'REL'

    def close_enough(self):
        a_words = self.a_chunk.words
        b_words = self.b_chunk.words
        acceptable = len(a_words.split())
        return compare_words.close_enough(a_words, b_words, acceptable)

    def annotation_types_for(self, sentence):
        result = []
        chunk = self.a_chunk if sentence == self.a else self.b_chunk
        for annotation_set in sentence.annotations:
            for annotation in annotation_set:
                if annotation.string.lower() == chunk.words.lower():
                    result.append(annotation.type)
        return result

    def has_same_annotation_type(self):
        a_types = self.annotation_types_for(self.a)
        b_types = self.annotation_types_for(self.b)
        return len(Set(a_types).intersection(b_types)) != 0

    def __str__(self):
        if self.type == 'ALIC':
            score = '0'
        else:
            score = repr(int(round(self.score * 5)))
        a_numbers = b_numbers = '0'
        left = right = '-not aligned-'
        if self.a_chunk:
            a_numbers = str(self.a_chunk.numbers())
            left = unicode(self.a_chunk)
        if self.b_chunk:
            b_numbers = str(self.b_chunk.numbers())
            right = unicode(self.b_chunk)
        return u' // '.join((
            a_numbers + ' <==> ' + b_numbers,
            self.type, score,
            u' <==> '.join((left, right))
        ))
