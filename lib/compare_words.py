from sets import Set
from textblob import Word
import Levenshtein
import word2vec


SCORE_WEIGHTS = {
    'PATH': 0.0,
    'WUP': 0.75,
    'WORD2VEC': 0.25
}


def edit_distance(a, b):
    return Levenshtein.distance(a, b)


def close_enough(a, b, acceptable):
    a_lemma = Word(a).lemmatize().lower()
    b_lemma = Word(b).lemmatize().lower()
    return edit_distance(a_lemma, b_lemma) <= acceptable


def synset_overlap(a, b):
    return Set(Word(a).synsets).intersection(Word(b).synsets)


def has_synset_overlap(a, b):
    return len(synset_overlap(a, b)) != 0


def with_path_similarity(a, b):
    result = None
    a_synsets = Word(a).synsets
    b_synsets = Word(b).synsets
    for a_syn in a_synsets:
        for b_syn in b_synsets:
            result = max(a_syn.path_similarity(b_syn), result)
    return result


def with_wup_similarity(a, b):
    result = None
    a_synsets = Word(a).synsets
    b_synsets = Word(b).synsets
    for a_syn in a_synsets:
        for b_syn in b_synsets:
            result = max(a_syn.wup_similarity(b_syn), result)
    return result


def with_word2vec(a, b):
    return word2vec.compare(a, b)


def score(a, b):
    path = with_path_similarity(a, b)
    wup = with_wup_similarity(a, b)
    w2v = with_word2vec(a, b)
    result = max_score = 0.0
    if path is not None:
        result += path * SCORE_WEIGHTS['PATH']
        max_score += SCORE_WEIGHTS['PATH']
    if wup is not None:
        result += wup * SCORE_WEIGHTS['WUP']
        max_score += SCORE_WEIGHTS['WUP']
    if w2v is not None:
        result += abs(w2v) * SCORE_WEIGHTS['WORD2VEC']
        max_score += SCORE_WEIGHTS['WORD2VEC']
    assert(0 <= result <= 1)
    return result
