class Chunk(object):

    def __init__(self, index, words):
        self.start_index = index
        self.words = words

    def __str__(self):
        return self.words

    def __repr__(self):
        return '[ ' + str(self) + ' ]'

    def numbers(self):
        range = xrange(self.start_index,
                       self.start_index + len(self.words.split()))
        return ' '.join([str(i + 1) for i in range])
