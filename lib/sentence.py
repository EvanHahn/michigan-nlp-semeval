from textblob import TextBlob


class Sentence(object):

    def __init__(self, plain):
        self.plain = plain
        self.textblob = TextBlob(plain)
        self.chunks = []
        self.annotations = []

    def __str__(self):
        return self.plain

    def __repr__(self):
        return str(self)
